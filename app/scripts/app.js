/// <reference path="../typings/node-webkit/node-webkit.d.ts" />
/// <reference path="../typings/polymer-x/polymer.d.ts" />
var gui = require("nw.gui");
(function (document) {
    'use strict';
    gui.Window.get().showDevTools();
    document.addEventListener("polymer-ready", function () {
        console.log('Polymer is ready to rock!');
    });
})(wrap(document));
