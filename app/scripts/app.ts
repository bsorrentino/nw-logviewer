/// <reference path="../typings/node-webkit/node-webkit.d.ts" />
/// <reference path="../typings/polymer-x/polymer.d.ts" />
import gui = require("nw.gui");

(function(document) {
  'use strict';

    // NW SECTION
    gui.Window.get().showDevTools();

    (<HTMLElement>document).addEventListener( "polymer-ready", ()=> {
        console.log('Polymer is ready to rock!');
    });


// wrap document so it plays nice with other libraries
// http://www.polymer-project.org/platform/shadow-dom.html#wrappers
})(wrap(document));
