/// <reference path="../../typings/polymer-x/polymer.d.ts" />
var CSearchButton = (function () {
    function CSearchButton() {
        this.placeholder = "enter search item ....";
        this.publish = { item: "" };
    }
    Object.defineProperty(CSearchButton.prototype, "_ply", {
        get: function () { return this; },
        enumerable: true,
        configurable: true
    });
    CSearchButton.prototype.ready = function () {
        var self;
    };
    CSearchButton.prototype.onSearch = function (e) {
        console.log("" + this.item),
            this._ply.fire('search', { 'item': this.item });
    };
    CSearchButton.prototype.onClear = function (e) {
        console.log("reset");
        this.item = '';
        this._ply.fire('reset', {});
    };
    return CSearchButton;
})();
Polymer("bsc-search-button", CSearchButton.prototype);
