/// <reference path="../../typings/polymer-x/polymer.d.ts" />

///
///
///
class CSearchButton implements IPolymerElement  {

  // Bridge property
  get _ply():CPolymerElement { return <any>this; }

  placeholder = "enter search item ....";

  publish = { item:"" };

  item:string;

  ready():void {
     var self:HTMLElement;

  }

  onSearch(e:Object):void {
    console.log( "" +this.item),
    this._ply.fire( 'search', { 'item': this.item } );
  }

  onClear(e:Object):void {
    console.log( "reset" )
    this.item = '';
    this._ply.fire( 'reset', { } );


  }
}


Polymer( "bsc-search-button", CSearchButton.prototype)
