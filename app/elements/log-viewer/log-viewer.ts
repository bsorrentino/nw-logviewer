/// <reference path="../../typings/node/node.d.ts" />

interface InfinityList {

  appendEl( el:any );
  appendModel( model:any );
  refresh( force:boolean );

}

interface LogLine {
  line:string;
  index?:number;
}

class LogViewer implements IPolymerElement {

  lines:Array<LogLine>;

  filters:any;
  filter:any;

  fd:any = null;

  // Bridge property
  get _ply():CPolymerElement { return <any>this; }


  get logSearch():InfinityList {
    return <InfinityList>this._ply.$['logSearch'];
  }
  get logList():InfinityList {
    return <InfinityList>this._ply.$['logList'];
  }
  get search():CSearchButton {
    return <CSearchButton>this._ply.$['search'];
  }

  ready() {

      this.lines = new Array<LogLine>();
      this.filters = { datePattern:'' };
      this.filter = { on: false };

      this.startWatch()

  }

  private _appendLine( line:string) {

    var l:LogLine = {line:line};
    l.index = this.lines.push( l );
    this.logList.appendModel( l );

    if( this.filter.on ) this._addSearchResult( line, this.search.item );
  }

  private _addSearchResult( l:string, line:string ) {

    var re:RegExp = new RegExp( line, "i");

    var m:Array<String> = l.split( re );
    if( m && m.length > 1) {
      var ll:LogLine = {line:""}, tk:number;
      var from:number = 0;
      for( tk = 0 ; tk < m.length-1 ; ++tk ) {
              from += m[tk].length;
          ll.line += m[tk] +
              "<span class='token'>" +
              l.substr(from,line.length) +
              "</span>";
      }
      ll.line += m[tk];
      this.logSearch.appendEl( "<div class='line'>"+ll.line+"</div>"  );

    }
  }

  startWatch() {
    if( !this.fd ) return ;

    var ft  = require('file-tail').startTailing( {
      fd:this.fd, //"/private/var/log/system.log",
      interval:1000
    });


    ft.on('tailError', (error)=> {
      console.log( "tail error reading log", error );
    });

    ft.on('error', (error)=> {
      console.log( "error reading log", error );
    });

    ft.on('line', (line)=> {
      console.log("line", line, this);

      this._appendLine( line );

    });

  }

  onSearch(e:any) {

    console.log("onSearch", e);
    //this.fire( "tap-search", {event:e, data:''} );
    process.nextTick( ()=> {

      //self.filter.rex = new RegExp( e.detail.item, 'gi' );
      var m,l;
      for( var i in this.lines ) {
        l = this.lines[i].line;

        this._addSearchResult( l, e.detail.item );

      }
    })
    this.filter.on = true;
  }

  onSearchReset(e:any) {

      console.log("onSearchReset", e);

      this.search.item = '';
      this.filter.on = false;

      var l = this.logSearch;

      setTimeout( ()=> {
          console.log( "refresh view", l);
          l.refresh(true);
      }, 300 );
  }

  attributeChanged(attrName:string, oldVal:any, newVal:any) {
     if( attrName === "fd" && oldVal===null && newVal!==null ) {
         this.startWatch();
     }
   }


}


Polymer( "log-viewer", LogViewer.prototype)
