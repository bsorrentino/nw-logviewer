
var ft  = require('file-tail').startTailing( {
  fd:"/private/var/log/system.log",
  interval:1000
});


ft.on('tailError', function(error) {
  console.log( "tail error reading log", error );
});

ft.on('error', function(error) {
  console.log( "error reading log", error );
});

ft.on('line', function(line) {
  console.log("line", line);
});

ft.on('stream', function(stream) {
  console.log("stream", stream);
});
