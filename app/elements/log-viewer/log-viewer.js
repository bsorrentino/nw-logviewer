/// <reference path="../../typings/node/node.d.ts" />
var LogViewer = (function () {
    function LogViewer() {
        this.fd = null;
    }
    Object.defineProperty(LogViewer.prototype, "_ply", {
        get: function () { return this; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogViewer.prototype, "logSearch", {
        get: function () {
            return this._ply.$['logSearch'];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogViewer.prototype, "logList", {
        get: function () {
            return this._ply.$['logList'];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogViewer.prototype, "search", {
        get: function () {
            return this._ply.$['search'];
        },
        enumerable: true,
        configurable: true
    });
    LogViewer.prototype.ready = function () {
        this.lines = new Array();
        this.filters = { datePattern: '' };
        this.filter = { on: false };
        this.startWatch();
    };
    LogViewer.prototype._appendLine = function (line) {
        var l = { line: line };
        l.index = this.lines.push(l);
        this.logList.appendModel(l);
        if (this.filter.on)
            this._addSearchResult(line, this.search.item);
    };
    LogViewer.prototype._addSearchResult = function (l, line) {
        var re = new RegExp(line, "i");
        var m = l.split(re);
        if (m && m.length > 1) {
            var ll = { line: "" }, tk;
            var from = 0;
            for (tk = 0; tk < m.length - 1; ++tk) {
                from += m[tk].length;
                ll.line += m[tk] +
                    "<span class='token'>" +
                    l.substr(from, line.length) +
                    "</span>";
            }
            ll.line += m[tk];
            this.logSearch.appendEl("<div class='line'>" + ll.line + "</div>");
        }
    };
    LogViewer.prototype.startWatch = function () {
        var _this = this;
        if (!this.fd)
            return;
        var ft = require('file-tail').startTailing({
            fd: this.fd,
            interval: 1000
        });
        ft.on('tailError', function (error) {
            console.log("tail error reading log", error);
        });
        ft.on('error', function (error) {
            console.log("error reading log", error);
        });
        ft.on('line', function (line) {
            console.log("line", line, _this);
            _this._appendLine(line);
        });
    };
    LogViewer.prototype.onSearch = function (e) {
        var _this = this;
        console.log("onSearch", e);
        process.nextTick(function () {
            var m, l;
            for (var i in _this.lines) {
                l = _this.lines[i].line;
                _this._addSearchResult(l, e.detail.item);
            }
        });
        this.filter.on = true;
    };
    LogViewer.prototype.onSearchReset = function (e) {
        console.log("onSearchReset", e);
        this.search.item = '';
        this.filter.on = false;
        var l = this.logSearch;
        setTimeout(function () {
            console.log("refresh view", l);
            l.refresh(true);
        }, 300);
    };
    LogViewer.prototype.attributeChanged = function (attrName, oldVal, newVal) {
        if (attrName === "fd" && oldVal === null && newVal !== null) {
            this.startWatch();
        }
    };
    return LogViewer;
})();
Polymer("log-viewer", LogViewer.prototype);
