// Type definitions for polymer
// Project: https://github.com/polymer
// Definitions by: Louis Grignon <https://github.com/lgrignon>
// Definitions: https://github.com/borisyankov/DefinitelyTyped


declare function wrap( e:any ): any;

interface IPolymerElement  {

	// definition
	publish?: Object;
	computed?: Object;
	// object mapping variable names to functions name
	observe?: Object;

	// life time API
	created? (): void;
	ready? (): void;
	attached? (): void;
	domReady? (): void;
	detached? (): void;
	attributeChanged? (attrName: string, oldVal: any, newVal: any): void;
}



interface IPolymer {


    importElements(node: Node, callback: Function): void;
    import(url: string, callback?: () => void): void;

    mixin(target: any, ...mixins: any[]): any;
    waitingFor(): Array<string>;
    // should be an "integer" for milliseconds
    forceReady(timeout: number): void;

    (tagName: string, prototype: PolymerElement): void;
    (tagName: string, prototype: any): void;
    (prototype: PolymerElement): void;
    (): void;
	// hacks for mixins
    CoreResizer: any;
    CoreResizable: any;
}

declare class CPolymerElement implements IPolymerElement {

	$: { [id: string]: any }; //polymer object for elements that have an ID

	// inargs is the [args] for the callback.. need to update function def
	async(inMethod: () => void, inArgs?: Array<any>, inTimeout?: number): void ;
	job(jobName: string, inMethod: () => void, inTimeout?: number): void ;
	fire(eventName: string, details?: any, targetNode?: any, bubbles?: boolean, cancelable?: boolean): void ;
	asyncFire(eventName: string, details?: any, targetNode?: any, bubbles?: boolean, cancelable?: boolean): void ;

	cancelUnbindAll(): void ;
}


declare var Polymer: IPolymer;
